import * as THREE from 'https://threejsfundamentals.org/threejs/resources/threejs/r122/build/three.module.js';
import {OrbitControls} from 'https://threejsfundamentals.org/threejs/resources/threejs/r119/examples/jsm/controls/OrbitControls.js';

function main() {
  const canvas = document.querySelector('#c');
  const renderer = new THREE.WebGLRenderer({canvas});

  const fov = 75;
  const aspect = 2;  // the canvas default
  const near = 0.1;
  const far = 100;
  const camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
  camera.position.z = 5;

  const controls = new OrbitControls(camera, canvas);
  controls.target.set(0, 0, 0);
  controls.update();

  const scene = new THREE.Scene();
  scene.background = new THREE.Color( 0x606060 ); // set background

  const loader = new THREE.TextureLoader();
  const firstTexture = loader.load('firstTexture.jpg');
  firstTexture.wrapS = firstTexture.wrapT = THREE.MirroredRepeatWrapping;
  firstTexture.repeat.set(3, 3);

  const secondTexture = loader.load('secondTexture.png');
  secondTexture.wrapS = secondTexture.wrapT = THREE.MirroredRepeatWrapping;
  secondTexture.repeat.set(3, 3);

  const materials = [
      new THREE.MeshPhongMaterial({map: firstTexture}),
      new THREE.MeshPhongMaterial({map: secondTexture, specular: 0xbbbbbb}),
  ];

  {
    const color = 0xFFFFFF;
    const intensity = 1;
    const light = new THREE.DirectionalLight(color, intensity);
    const secondLight = new THREE.DirectionalLight(color, intensity);
    light.position.set(-1, 2, 4);
    secondLight.position.set(1, 2, 4);
    scene.add(light);
    scene.add(secondLight);
  }

  const geometry = new THREE.Geometry();
  geometry.vertices.push(
    new THREE.Vector3(0, 0,  0.5),  // 0
    new THREE.Vector3(-0.25, 0.25,  0),  // 1
    new THREE.Vector3(0,  1,  0),  // 2
    new THREE.Vector3(0.25,  0.25,  0),  // 3
    new THREE.Vector3(1, 0, 0),  // 4
    new THREE.Vector3(0.25, -0.25, 0),  // 5
    new THREE.Vector3(0.5, -1, 0),  // 6
    new THREE.Vector3(0,  -0.5, 0),  // 7
    new THREE.Vector3(-0.5,  -1, 0),  // 8
    new THREE.Vector3(-0.25,  -0.25, 0),  // 9
    new THREE.Vector3(-1, 0, 0),  // 10
    new THREE.Vector3(0, 0, -0.25), //11
  );

  /*
 front
        2
   10_1/_\3__4
     '9 0 5.'
     /.'7'.\
    8'     '6
 back
        2
   4__3/_\1__10
     '5.11 9.'
     /.'7'.\
    6'     '8
  */

  geometry.faces.push(
     // up
     new THREE.Face3(0, 3, 2),
     new THREE.Face3(0, 2, 1),
     // right
     new THREE.Face3(0, 4, 3),
     new THREE.Face3(0, 5, 4),
     // lower right
     new THREE.Face3(0, 6, 5),
     new THREE.Face3(0, 7, 6),
     // lower left
     new THREE.Face3(0, 9, 8),
     new THREE.Face3(0, 8, 7),
     // left
     new THREE.Face3(0, 1, 10),
     new THREE.Face3(0, 10, 9),
     //b up
     new THREE.Face3(11, 1, 2),
     new THREE.Face3(11, 2, 3),
     //b right
     new THREE.Face3(11, 10, 1),
     new THREE.Face3(11, 9, 10),
     //b low right
     new THREE.Face3(11, 8, 9),
     new THREE.Face3(11, 7, 8),
     //b low left
     new THREE.Face3(11, 6, 7),
     new THREE.Face3(11, 5, 6),
     //b left
     new THREE.Face3(11, 3, 4),
     new THREE.Face3(11, 4, 5),
  );

  geometry.faces.forEach(function (face3, i) {
    if(i < 10) {
      face3.materialIndex = 0;
    }
    else {
      face3.materialIndex = 1;
    }
  });

  geometry.faces.forEach((face, ndx) => {
    face.vertexColors = [
      (new THREE.Color()).setHSL(ndx / 12      , 1, 0.5),
      (new THREE.Color()).setHSL(ndx / 12 + 0.1, 1, 0.5),
      (new THREE.Color()).setHSL(ndx / 12 + 0.2, 1, 0.5),
    ];
  });

  function assignUVs(geometry) {
    geometry.faceVertexUvs[0] = [];
    geometry.faces.forEach(function(face) {
        var components = ['x', 'y', 'z'].sort(function(a, b) {
            return Math.abs(face.normal[a]) > Math.abs(face.normal[b]);
        });

        var v1 = geometry.vertices[face.a];
        var v2 = geometry.vertices[face.b];
        var v3 = geometry.vertices[face.c];

        geometry.faceVertexUvs[0].push([
            new THREE.Vector2(v1[components[0]], v1[components[1]]),
            new THREE.Vector2(v2[components[0]], v2[components[1]]),
            new THREE.Vector2(v3[components[0]], v3[components[1]])
        ]);

    });
    geometry.uvsNeedUpdate = true;
}

  geometry.computeFaceNormals();
  //https://jsfiddle.net/benaloney/L7js807k/9/
  assignUVs(geometry);

  function makeInstance(geometry, color, x) {
    const cube = new THREE.Mesh(geometry, materials);
    scene.add(cube);

    cube.position.x = x;
    return cube;
  }

  const cubes = [
    makeInstance(geometry, 0x44FF44,  0),
    makeInstance(geometry, 0x4444FF, -4),
    makeInstance(geometry, 0xFF4444,  4),
  ];

  function resizeRendererToDisplaySize(renderer) {
    const canvas = renderer.domElement;
    const width = canvas.clientWidth;
    const height = canvas.clientHeight;
    const needResize = canvas.width !== width || canvas.height !== height;
    if (needResize) {
      renderer.setSize(width, height, false);
    }
    return needResize;
  }

  function render(time) {
    time *= 0.001;

    if (resizeRendererToDisplaySize(renderer)) {
      const canvas = renderer.domElement;
      camera.aspect = canvas.clientWidth / canvas.clientHeight;
      camera.updateProjectionMatrix();
    }

    cubes.forEach((cube, ndx) => {
      const speed = 1 + ndx * .1;
      const rot = time * speed;
      cube.rotation.x = rot;
      cube.rotation.y = rot;
    });

    renderer.render(scene, camera);

    requestAnimationFrame(render);
  }

  requestAnimationFrame(render);
}

main();